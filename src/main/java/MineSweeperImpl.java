public class MineSweeperImpl implements MineSweeper{

    private String[][] mines;

    public void setMineField(String mineField) throws IllegalArgumentException {
        String [] mineLines = mineField.split("\\\\n");

        for(int i = 0; i < mineLines.length-1; i++){
            if(mineLines[i].length() != mineLines[i+1].length()){
                throw new IllegalArgumentException("Mine fields has a wrong format");
            }
        }

        this.mines = new String [mineLines.length][mineLines[0].length()];
        for(int i = 0; i < mineLines.length; i++) {
            for(int j = 0; j < mineLines[i].toCharArray().length; j++) {
                this.mines[i][j] = String.valueOf(mineLines[i].charAt(j));
            }
        }
    }

    public String getHintField() throws IllegalStateException {
        if(this.mines == null){
            throw new IllegalStateException("Mines are not initialized");
        }
        int counter = 0;
        String result = "";
        for (int i = 0; i < this.mines.length; i++) {
            for (int j = 0; j < this.mines[i].length; j++) {
                if (this.mines[i][j].equals("*")) {
                    result += this.mines[i][j];
                }
                if (i < this.mines.length - 1 && this.mines[i + 1][j].equals("*")) {
                    counter++;
                }
                if (i > 0 && this.mines[i - 1][j].equals("*")) {
                    counter++;
                }
                if (j > 0 && this.mines[i][j - 1].equals("*")) {
                    counter++;
                }
                if (j < this.mines[i].length - 1 && this.mines[i][j + 1].equals("*")) {
                    counter++;
                }
                if (i < this.mines.length - 1 && j < this.mines.length - 1 && this.mines[i + 1][j + 1].equals("*")) {
                    counter++;
                }
                if (i > 0 && j < this.mines.length - 1 && this.mines[i - 1][j + 1].equals("*")) {
                    counter++;
                }
                if (i > 0 && j > 0 && this.mines[i - 1][j - 1].equals("*")) {
                    counter++;
                }
                if (i < this.mines.length - 1 && j > 0 && this.mines[i + 1][j - 1].equals("*")) {
                    counter++;
                }

                if (!this.mines[i][j].equals("*")) {
                    result += counter;
                }
                counter = 0;
            }
        }
        return result;
    }
}
