public class Main {
    public static void main(String[] args) {
        MineSweeperImpl mineSweeper = new MineSweeperImpl();
        mineSweeper.setMineField("*...\\n..*.\\n....");
        String hintField = mineSweeper.getHintField();
        System.out.println("Hint field " + hintField);
    }
}
